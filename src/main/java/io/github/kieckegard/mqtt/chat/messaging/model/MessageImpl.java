/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.messaging.model;

import java.util.UUID;

/**
 *
 * @author kieckegard
 */
public class MessageImpl implements Message {
    
    private String id;
    private String topic;
    private String message;
    private String from;

    public MessageImpl(String topic, String message, String from) {
        this.id = generateId();
        this.topic = topic;
        this.message = message;
        this.from = from;
    }
    
    private String generateId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    @Override
    public boolean wasYou(String clientId) {
        return clientId.equals(this.from);
    }

    @Override
    public String toString() {
        return "MessageImpl{" + "id=" + id + ", topic=" + topic + ", message=" + message + ", from=" + from + '}';
    }
}
