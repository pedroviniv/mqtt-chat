/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.repository;

/**
 *
 * @author kieckegard
 */
public class MessagesReceivedFactory {
    
    private volatile static MessagesReceived messagesReceived;
    
    public MessagesReceived getInstance() {
        MessagesReceived result = messagesReceived;
        if(result == null) {
            synchronized(this) {
                result = messagesReceived;
                if(result == null) {
                    messagesReceived = result = new MessagesReceivedMemoryImpl();
                }
            }
        }
        return result;
    }
}
