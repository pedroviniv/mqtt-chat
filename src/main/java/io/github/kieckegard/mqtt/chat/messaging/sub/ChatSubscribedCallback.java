/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.messaging.sub;

import io.github.kieckegard.mqtt.chat.repository.MessagesReceived;
import io.github.kieckegard.mqtt.chat.messaging.model.Message;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import io.github.kieckegard.mqtt.chat.messaging.Subject;
import io.github.kieckegard.mqtt.chat.messaging.adapter.MqttMessageAdapter;

/**
 *
 * @author kieckegard
 */
public class ChatSubscribedCallback 
        implements MqttCallback, Subject<TopicListener, Message> {

    private final MessagesReceived messagesReceived;
    private final List<TopicListener> topicListeners = new ArrayList<>();
    private final String clientId;

    public ChatSubscribedCallback(
            MessagesReceived messagesReceived, String clientId) {
        
        this.messagesReceived = messagesReceived;
        this.clientId = clientId;
    }

    @Override
    public void connectionLost(Throwable thrwbl) {
        System.out.println("Connection to MQTT Broker was lost!");
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) 
            throws Exception {
        //adapting mqttMessage received to the message interface
        Message message = new MqttMessageAdapter(mqttMessage, topic);
        //persisting message
        this.messagesReceived.add(message);
        //notifying all topic listeners
        this.sendAll(message);
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken imdt) {
        //
    }

    @Override
    public void sendAll(Message message) {
        this.topicListeners
                .stream()
                .filter(t -> t.getTopic().equals(message.getTopic()))
                .forEach(t -> t.onMessage(message));
    }

    @Override
    public void add(TopicListener topicListener) {
        this.topicListeners.add(topicListener);
    }

    @Override
    public void remove(TopicListener topicListener) {
        this.topicListeners.remove(topicListener);
    }

}
