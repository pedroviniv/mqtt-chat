/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.messaging.model;

/**
 *
 * @author pafer
 */
public interface Message {

    String getFrom();

    String getId();

    String getMessage();

    String getTopic();
    
    boolean wasYou(String clientId);
    
}
