/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.messaging.pub;

import io.github.kieckegard.mqtt.chat.messaging.MQTTInitializeException;
import io.github.kieckegard.mqtt.chat.messaging.Token;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author kieckegard
 */
public class MqttMessageSender implements MessageSender {
    
    private MqttClient client;
    private final String clientId;
    private final String tcpAddress;
    
    public MqttMessageSender(String tcpAddress, String clientEmail) {
        this.tcpAddress = tcpAddress;
        this.clientId = getClientId(clientEmail);
        connect();
    }
    
    private String getClientId(String email) {
        return "pub" + email;
    }
    
    private void connect() {
        try {
            this.client = new MqttClient(this.tcpAddress, this.clientId);
            this.client.connect();
        } catch (MqttException ex) {
            throw new MQTTInitializeException("An error ocurred"
                    + " while instantiating the client.", ex);
        }
    }

    @Override
    public void send(String topic, String text) {
        
        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder
                .append(Token.CLIENT_ID_DELIMITER)
                .append(this.clientId)
                .append(Token.CLIENT_ID_DELIMITER)
                .append(text);
        
        String message = messageBuilder.toString();
        
        try {
            this.client.publish(topic, new MqttMessage(message.getBytes()));
        } catch (MqttException ex) {
            throw new MessageSendingException("An error ocurred "
                    + "while sending the msg " + text, ex);
        }
    }

    public String getClientId() {
        return clientId;
    }

    public String getTcpAddress() {
        return tcpAddress;
    }
}
