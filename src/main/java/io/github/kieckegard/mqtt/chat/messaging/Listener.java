/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.messaging;

/**
 *
 * @author kieckegard
 */
public interface Listener<T> {
    
    void onMessage(T obj);
}
