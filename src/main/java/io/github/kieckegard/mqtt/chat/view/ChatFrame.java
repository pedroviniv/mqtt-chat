/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.view;

import io.github.kieckegard.mqtt.chat.messaging.model.Message;
import io.github.kieckegard.mqtt.chat.messaging.pub.MessageSender;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import io.github.kieckegard.mqtt.chat.messaging.sub.TopicListener;

/**
 *
 * @author kieckegard
 */
public class ChatFrame extends javax.swing.JFrame implements TopicListener {
    
    private final MessageSender sender;
    private final String topic;
    private DefaultListModel listModel = new DefaultListModel();

    public ChatFrame(String topic, MessageSender sender) {
        this.topic = topic;
        this.sender = sender;
        initComponents();
        this.topicName.setText(topic);
        center();
        configurePanelChatSize();
        centerChatTile();
    }
    
    private void configurePanelChatSize() {
        Dimension chatTitleWrapper = this.chatTitleWrapper.getSize();
        double lblTopicWidth = lblTopic.getSize().getWidth();
        double topicNameWidth = this.topicName.getSize().getWidth();
        double marginRight = 10;
        double width = lblTopicWidth + marginRight + topicNameWidth;

        chatTitleWrapper.setSize(width, chatTitleWrapper.getHeight());
        
        this.chatTitleWrapper.setSize(chatTitleWrapper);
    }
    
    private void centerChatTile() {
        Dimension panelChatTitleDimension = this.panelChatTitle.getSize();
        
        this.chatTitleWrapper
                .setLocation(panelChatTitleDimension.width/2-chatTitleWrapper
                        .getSize().width/2, 
                        panelChatTitleDimension.height/2-chatTitleWrapper
                                .getSize().height/2);
    }
    
    private void center() {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
    }
    
    @Override
    public void onMessage(Message obj) {
        System.out.println("message: " + obj);
        listModel.addElement(String.format("%s: %s", obj.getFrom(), obj.getMessage()));
        this.topicMsgs.setModel(listModel);
    }
    
    @Override
    public String getTopic() {
        return this.topic;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelChatTitle = new javax.swing.JPanel();
        chatTitleWrapper = new javax.swing.JPanel();
        topicName = new javax.swing.JLabel();
        lblTopic = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        topicMsgs = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        msgText = new javax.swing.JTextArea();
        btnSendMsg = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panelChatTitle.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        topicName.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 18)); // NOI18N
        topicName.setText("Topic name");

        lblTopic.setFont(new java.awt.Font("Microsoft JhengHei UI Light", 0, 18)); // NOI18N
        lblTopic.setForeground(new java.awt.Color(123, 169, 208));
        lblTopic.setText("CHAT");

        javax.swing.GroupLayout chatTitleWrapperLayout = new javax.swing.GroupLayout(chatTitleWrapper);
        chatTitleWrapper.setLayout(chatTitleWrapperLayout);
        chatTitleWrapperLayout.setHorizontalGroup(
            chatTitleWrapperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chatTitleWrapperLayout.createSequentialGroup()
                .addComponent(lblTopic)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(topicName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        chatTitleWrapperLayout.setVerticalGroup(
            chatTitleWrapperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chatTitleWrapperLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(chatTitleWrapperLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(topicName)
                    .addComponent(lblTopic)))
        );

        javax.swing.GroupLayout panelChatTitleLayout = new javax.swing.GroupLayout(panelChatTitle);
        panelChatTitle.setLayout(panelChatTitleLayout);
        panelChatTitleLayout.setHorizontalGroup(
            panelChatTitleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelChatTitleLayout.createSequentialGroup()
                .addGap(128, 128, 128)
                .addComponent(chatTitleWrapper, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelChatTitleLayout.setVerticalGroup(
            panelChatTitleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelChatTitleLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(chatTitleWrapper, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(topicMsgs);

        msgText.setColumns(20);
        msgText.setFont(new java.awt.Font("Microsoft JhengHei UI", 0, 13)); // NOI18N
        msgText.setRows(5);
        jScrollPane2.setViewportView(msgText);

        btnSendMsg.setText("Send");
        btnSendMsg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendMsgActionPerformed(evt);
            }
        });

        jLabel1.setText("Type your message:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelChatTitle, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
                    .addComponent(btnSendMsg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelChatTitle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSendMsg, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    private void btnSendMsgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendMsgActionPerformed
        String msgTextStr = this.msgText.getText();
        if(msgTextStr.isEmpty())
            JOptionPane.showMessageDialog(this, "You can't send an empty msg!", 
                    "Error", JOptionPane.INFORMATION_MESSAGE);
        else
            this.sender.send(topic, this.msgText.getText());
    }//GEN-LAST:event_btnSendMsgActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSendMsg;
    private javax.swing.JPanel chatTitleWrapper;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblTopic;
    private javax.swing.JTextArea msgText;
    private javax.swing.JPanel panelChatTitle;
    private javax.swing.JList<String> topicMsgs;
    private javax.swing.JLabel topicName;
    // End of variables declaration//GEN-END:variables

    

}
