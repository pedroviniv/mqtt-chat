/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.messaging;


/**
 *
 * @author kieckegard
 * @param <Listener>
 * @param <E>
 */
public interface Subject<Listener, E> {
    
    void add(Listener obj);
    void remove(Listener obj);
    void sendAll(E msg);
}
