/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.repository;

import io.github.kieckegard.mqtt.chat.messaging.model.Message;
import java.util.List;

/**
 *
 * @author kieckegard
 */
public interface MessagesReceived {

    void add(Message message);

    List<Message> listAll();

    boolean remove(String messageId);
    
}
