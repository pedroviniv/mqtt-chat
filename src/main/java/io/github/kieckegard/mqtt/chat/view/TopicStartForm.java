/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.view;

import io.github.kieckegard.mqtt.chat.messaging.sub.MQTTSubscribeClient;
import io.github.kieckegard.mqtt.chat.messaging.pub.MessageSender;
import io.github.kieckegard.mqtt.chat.messaging.pub.MqttMessageSender;
import io.github.kieckegard.mqtt.chat.helper.PropertiesReader;
import io.github.kieckegard.mqtt.chat.helper.PropertiesReaderImpl;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Properties;
import javax.swing.JOptionPane;

/**
 *
 * @author kieckegard
 */
public class TopicStartForm extends javax.swing.JFrame {
    
    private String email;
    private MQTTSubscribeClient subClient;
    private MessageSender pubClient;
    
    private final String brokerAddress;

    public TopicStartForm(String email) {
        initComponents();
        this.brokerAddress = loadBrokerAddress();
        System.out.println("broker address: " + brokerAddress);
        this.email = email;
        init();
        center();
    }
    
    private String loadBrokerAddress() {
        
        PropertiesReader reader = new PropertiesReaderImpl();
        Properties prop = reader.read("application.properties");
        return prop.getProperty("broker.address");
    }
    
    private void center() {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
    }
    
    private void init() {
        this.subClient = new MQTTSubscribeClient(this.brokerAddress, email);
        this.subClient.run();
        this.pubClient = new MqttMessageSender(this.brokerAddress, email);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        topicName = new javax.swing.JTextField();
        lblChatName = new javax.swing.JLabel();
        startChat = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        topicName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                topicNameActionPerformed(evt);
            }
        });

        lblChatName.setText("Chat name:");

        startChat.setText("Start Chat");
        startChat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startChatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(topicName)
                    .addComponent(startChat, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblChatName)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblChatName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(topicName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(startChat)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void topicNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_topicNameActionPerformed

    }//GEN-LAST:event_topicNameActionPerformed

    private void startChatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startChatActionPerformed
        String topic = this.topicName.getText();
        if(topic.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Please, fill the topic field below!", 
                    "Alert", JOptionPane.INFORMATION_MESSAGE);
        } else {
            this.subClient.subscribe(topic);
            ChatFrame chatFrame = new ChatFrame(topic, this.pubClient);
            this.subClient.getChatCallback().add(chatFrame);
            chatFrame.setVisible(true);
        }
    }//GEN-LAST:event_startChatActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblChatName;
    private javax.swing.JButton startChat;
    private javax.swing.JTextField topicName;
    // End of variables declaration//GEN-END:variables
}
