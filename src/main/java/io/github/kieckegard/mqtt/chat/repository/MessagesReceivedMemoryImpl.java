/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.repository;

import io.github.kieckegard.mqtt.chat.messaging.model.Message;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author kieckegard
 */
public class MessagesReceivedMemoryImpl implements MessagesReceived {
    
    public final List<Message> messages = Collections
            .synchronizedList(new ArrayList<>());
    
    @Override
    public void add(Message message) {
        this.messages.add(message);
    }
    
    @Override
    public synchronized boolean remove(String messageId) {
        return this.messages
                .removeIf(m -> m.getId().equals(messageId));
    }
    
    @Override
    public List<Message> listAll() {
        return Collections.unmodifiableList(this.messages);
    }
}
