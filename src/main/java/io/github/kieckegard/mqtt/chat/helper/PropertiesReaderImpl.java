/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author kieckegard
 */
public class PropertiesReaderImpl implements PropertiesReader {
    
    private String path;
    
    public PropertiesReaderImpl(String path) {
        this.path = path;
    }
    
    public PropertiesReaderImpl() {
        this.path = "./";
    }

    @Override
    public Properties read(String fileName) {
        try {
            FileInputStream fis = new FileInputStream(new File(path+fileName));
            Properties prop = new Properties();
            prop.load(fis);
            return prop;
        } catch (FileNotFoundException ex) {
            throw new PropertiesReaderException("An error ocurred"
                    + " while reading the property file!", ex);
        } catch (IOException ex) {
            throw new PropertiesReaderException("An error ocurred"
                    + " while reading the property file!", ex);
        }
    }
    
}
