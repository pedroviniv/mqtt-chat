/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.messaging;

/**
 *
 * @author kieckegard
 */
public class MQTTInitializeException extends RuntimeException {

    public MQTTInitializeException(String message) {
        super(message);
    }

    public MQTTInitializeException(String message, Throwable cause) {
        super(message, cause);
    }

    public MQTTInitializeException(Throwable cause) {
        super(cause);
    }
}
