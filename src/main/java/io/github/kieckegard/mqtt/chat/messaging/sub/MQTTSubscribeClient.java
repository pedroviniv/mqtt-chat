/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.messaging.sub;

import io.github.kieckegard.mqtt.chat.messaging.MQTTInitializeException;
import io.github.kieckegard.mqtt.chat.repository.MessagesReceived;
import io.github.kieckegard.mqtt.chat.repository.MessagesReceivedFactory;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author kieckegard
 */
public class MQTTSubscribeClient implements Runnable {
    
    private MqttClient client;
    private final String tcpAddress;
    private final String clientId;
    private ChatSubscribedCallback chatCallback;
    
    public MQTTSubscribeClient(String tcpAddress, String clientEmail) {
        this.tcpAddress = tcpAddress;
        this.clientId = getClientId(clientEmail);
    }
    
    private String getClientId(String clientEmail) {
        return "sub" + clientEmail;
    }
    
    public void subscribe(String topic) {
        try {
            this.client.subscribe(topic);
        } catch (MqttException ex) {
            throw new ClientSubscribingException("Error while"
                    + " subscribing client " + this.clientId
                    + " in topic " + topic, ex);
        }
    }
    
    @Override
    public synchronized void run() {
        try {
            //Getting an instance of messagesReceived
            MessagesReceived messagesReceived = new MessagesReceivedFactory().getInstance();
            //instantiating a new mqtt client!
            this.client = new MqttClient(this.tcpAddress, this.clientId);
            //instantianting a new MqttCallback
            this.chatCallback = 
                    new ChatSubscribedCallback(messagesReceived, clientId);
            //setting callback in the instatiated mqtt client
            client.setCallback(chatCallback);
            //connecting to the mqtt broker
            client.connect();
            System.out.println("client " + clientId
                    + " was successfully connected, notifying threads.");
            this.notify();
        } catch (MqttException ex) {
            throw new MQTTInitializeException("An error ocurred while"
                    + " initializating the mqttClient", ex);
        }
    }
    
    public boolean isConnected() {
        return this.client.isConnected();
    }
    
    public MqttClient getClient() {
        return this.client;
    }

    public String getTcpAddress() {
        return tcpAddress;
    }

    public String getClientId() {
        return clientId;
    }

    public ChatSubscribedCallback getChatCallback() {
        return chatCallback;
    }
}
