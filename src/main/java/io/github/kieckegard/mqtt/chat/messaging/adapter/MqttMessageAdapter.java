/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.messaging.adapter;

import io.github.kieckegard.mqtt.chat.messaging.model.Message;
import io.github.kieckegard.mqtt.chat.messaging.model.MessageImpl;
import io.github.kieckegard.mqtt.chat.messaging.Token;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author pafer
 */
public class MqttMessageAdapter implements Message {
    
    private final Message message;
    
    public MqttMessageAdapter(MqttMessage mqttMessage, String topic) {
        this.message = this.getInstance(mqttMessage, topic);
    }
    
    private Message getInstance(MqttMessage mqttMessage, String topic) {
        
        String msgPayload = new String(mqttMessage.getPayload());
        String[] idAndText = separateClientIdFrom(msgPayload);
        String from = idAndText[0].replaceAll("pub", "");
        String msgText = idAndText[1];
        
        return new MessageImpl(topic, msgText, from);
    }
    
    private String[] separateClientIdFrom(String msgText) {
        
        int lastOccurrence = msgText.lastIndexOf(Token.CLIENT_ID_DELIMITER)
                + Token.CLIENT_ID_DELIMITER.length();
        String idWithToken = msgText.substring(0, lastOccurrence);

        String[] args = new String[2];
        args[0] = idWithToken.replaceAll(Token.CLIENT_ID_DELIMITER, "");
        args[1] = msgText.replaceAll(idWithToken, "");

        return args;
    } 

    @Override
    public String getFrom() {
        return this.message.getFrom();
    }

    @Override
    public String getId() {
        return this.message.getId();
    }

    @Override
    public String getMessage() {
        return this.message.getMessage();
    }

    @Override
    public String getTopic() {
        return this.message.getTopic();
    }
    
    @Override
    public boolean wasYou(String clientId) {
        return message.wasYou(clientId);
    }
}
