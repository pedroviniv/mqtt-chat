/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.helper;

/**
 *
 * @author kieckegard
 */
public class PropertiesReaderException extends RuntimeException {

    public PropertiesReaderException(String message) {
        super(message);
    }

    public PropertiesReaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public PropertiesReaderException(Throwable cause) {
        super(cause);
    }
}
