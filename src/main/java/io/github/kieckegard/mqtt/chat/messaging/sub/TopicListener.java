/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.mqtt.chat.messaging.sub;

import io.github.kieckegard.mqtt.chat.messaging.model.Message;
import io.github.kieckegard.mqtt.chat.messaging.Listener;

/**
 *
 * @author kieckegard
 */
public interface TopicListener extends Listener<Message> {
    
    String getTopic();
}
