# Simple mqtt chat

This simple chat app was developed in Java and with the help of eclipse paho library, the app uses mqtt protocol v3 to handle all publish and subscribe calls to the mqtt broker.

The user interface was simply made with Java Swing and the messages persistence is made, by default, in files.

This project contains an `application.properties` file which contains the mqtt broker address. Just change it to your own mqtt broker address.

Since this project was built with Maven, in due to generate the jar file, follow the steps described below:

- be sure you have git and maven installed in your computer;
- open your best terminal, navigate to the folder which you want to download this project and execute the following commands:
	- `git clone https://pedroviniv@bitbucket.org/pedroviniv/mqtt-chat.git`
	- `mvn install ./mqtt-chat`

If the execution of the commands above went fine, a folder named "target" will be generated in the "mqtt-chat" folder. Finally, inside the "target" folder, you will find the generated jar with dependencies. 

navigate to the "target" folder and execute the jar file by running the command `java -jar mqtt-example.jar`.

Enjoy it.





